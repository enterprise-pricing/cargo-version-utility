//! This is small Cargo subcommand to manipulate Cargo.toml versions.

use cargo_metadata::{Metadata, Package};
use std::error::Error;
use std::{fs, process};
use toml_edit::{value, Document};

use clap::{Parser, Subcommand};

#[derive(Parser)]
#[clap(name = "cargo")]
#[clap(bin_name = "cargo")]
pub(crate) enum Cargo {
    VersionUtil(Arguments),
}

#[derive(clap::Args)]
#[clap(
    author,
    version,
    long_about = "\nCargo Version Utility\nManipulates Cargo.toml versions."
)]
struct Arguments {
    #[clap(subcommand)]
    commands: Commands,
}

#[derive(Subcommand)]
enum Commands {
    #[clap(about = "Sets the version on a Cargo project.")]
    SetVersion {
        #[clap(help = "The new version.")]
        version: String,
        #[clap(help = "The path to the Cargo.toml to edit.", long = "manifest-path")]
        manifest_path: Option<String>,
    },

    #[clap(about = "Gets the version of a Cargo project.")]
    GetVersion {
        #[clap(help = "The path to the Cargo.toml to query.", long = "manifest-path")]
        manifest_path: Option<String>,
    },

    #[clap(about = "Determines if the Cargo.toml is a workspace, exits with exit code 0 if true.")]
    IsWorkspace {
        #[clap(help = "The path to the Cargo.toml to query.", long = "manifest-path")]
        manifest_path: Option<String>,
    },
}

fn main() -> Result<(), Box<dyn Error>> {
    let Cargo::VersionUtil(arguments) = Cargo::parse();

    run_command(arguments)
}

fn run_command(arguments: Arguments) -> Result<(), Box<dyn Error>> {
    match arguments.commands {
        Commands::SetVersion {
            version,
            manifest_path,
        } => set_version(
            version,
            manifest_path.unwrap_or_else(|| "Cargo.toml".to_string()),
        ),
        Commands::GetVersion { manifest_path } => {
            get_version(manifest_path.unwrap_or_else(|| "Cargo.toml".to_string()))
        }
        Commands::IsWorkspace { manifest_path } => {
            let path = manifest_path.unwrap_or_else(|| "Cargo.toml".to_string());
            if is_workspace(&path) {
                process::exit(0);
            } else {
                process::exit(1);
            }
        }
    }
}

struct PathAndDoc {
    manifest_path: String,
    package: Package,
    toml: Document,
}

/// This creates a `Vec` containing the metadata and manifest of the root project.
/// If run on a workspace the result contains metadata and manifests of all workspace members.
///
/// ### Parameters:
///
/// * `manifest_path`: The Cargo.toml manifest path.
///
/// ### Returns:
/// [`Vec<PathAndDoc>`] for all the crates
///
fn read_manifests(manifest_path: &str) -> Result<Vec<PathAndDoc>, Box<dyn Error>> {
    let metadata = get_metadata(manifest_path);

    let reader = |package: &Package| {
        let package_manifest_path = package.manifest_path.as_str();

        let toml = read_toml_as_document(package_manifest_path).unwrap();
        PathAndDoc {
            manifest_path: package_manifest_path.to_string(),
            package: package.clone(),
            toml,
        }
    };

    if is_workspace(manifest_path) {
        let result = metadata
            .workspace_members
            .iter()
            .map(|id| {
                let package_data = &metadata[id];
                reader(package_data)
            })
            .collect();

        Ok(result)
    } else {
        let result = metadata
            .packages
            .iter()
            .map(|package| reader(package))
            .collect();
        Ok(result)
    }
}

/// Sets the specified version on the manifest at the given path.
///
/// ### Parameters:
///
/// * `version`: The version to set
/// * `manifest_path`: The Cargo.toml manifest path.
///
fn set_version(version: String, manifest_path: String) -> Result<(), Box<dyn Error>> {
    let read_manifests = read_manifests(&manifest_path)?;
    for path_and_doc in read_manifests.into_iter() {
        let mut doc = path_and_doc.toml;
        doc["package"]["version"] = value(&version);

        let output_path = &path_and_doc.manifest_path;
        fs::write(output_path, doc.to_string().as_bytes())?;
    }
    Ok(())
}

/// Gets the version from the manifest at the given path and prints it to std out.
///
/// ### Parameters:
///
/// * `manifest_path`: The Cargo.toml manifest path.
///
fn get_version(manifest_path: String) -> Result<(), Box<dyn Error>> {
    let read_manifests = read_manifests(&manifest_path)?;
    let mut versions: Vec<String> = read_manifests
        .iter()
        .map(|path_and_doc| path_and_doc.package.version.to_string())
        .collect();

    versions.sort();
    versions.dedup();

    if versions.len() != 1 {
        eprintln!("More than one unique version in the manifest/workspace");
        for path_and_doc in read_manifests.iter() {
            eprintln!(
                "{}: {}",
                path_and_doc.manifest_path, path_and_doc.package.version
            );
        }

        process::exit(1);
    } else {
        println!("{}", versions[0]);
        Ok(())
    }
}

/// Returns [`true`] if given manifest is a workspace manifest, [`false`] otherwise.
///
/// ### Parameters:
///
/// * `manifest_path`: The Cargo.toml manifest path.
///
fn is_workspace(manifest_path: &str) -> bool {
    let toml = read_toml_as_document(manifest_path).unwrap();
    toml.contains_table("workspace")
}

fn read_toml_as_document(toml_path: &str) -> Result<Document, Box<dyn Error>> {
    let file_content = fs::read_to_string(toml_path)?;
    match file_content.parse::<Document>() {
        Ok(it) => Ok(it),
        Err(err) => Err(err.into()),
    }
}

/// Get metadata for the current crate.
///
/// ### Parameters
///
/// * `manifest_path`: The Cargo.toml manifest path.
///
/// ### Returns:
///
/// [`Metadata`] for the crate.
///
fn get_metadata(manifest_path: &str) -> Metadata {
    let mut cmd = cargo_metadata::MetadataCommand::new();
    cmd.manifest_path(manifest_path);
    cmd.no_deps();
    let result = cmd.exec();
    if let Ok(metadata) = result {
        metadata
    } else {
        panic!("{}", result.err().unwrap());
    }
}
